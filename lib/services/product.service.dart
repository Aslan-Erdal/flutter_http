import 'dart:convert';

import 'package:flutter_http_requests/models/product.model.dart';
import 'package:http/http.dart';

class ProductService {
  // pour chrome
  static String productUrl = "http://localhost:8000/products.json";

  // pour emulateur
  // static String productUrl = "http://10.0.2.2:8000/products.json";

  static Future<ProductList?> getProducts() async {
     try {
       final Response response = await get(Uri.parse(productUrl), headers: {
        "Accept": "application/json"
       });
       if (response.statusCode == 200) {
         if(response.body.isNotEmpty) {
          final jsonResponse = json.decode(response.body); // maybe used jsonDecode()
          final ProductList products = ProductList.fromJson(jsonResponse);
          if (products.products.isNotEmpty) {
            return products;
          } else {
            return null;
          }
         } else {
          return null;
         }
       } else {
          throw Exception('Impossible de charger les produits');
       }
     } catch (e) {
       print(e.toString());
       throw Exception('Impossible de charger les produits');
     }
  }
}