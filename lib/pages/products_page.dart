import 'package:flutter/material.dart';
import 'package:flutter_http_requests/components/product_lister.dart';

class ProductsPage extends StatelessWidget {
  const ProductsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: const Text(
            'Nos Produits',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: const ProductLister(),
        );
  }
}
