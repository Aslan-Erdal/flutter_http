import 'package:flutter/material.dart';
import 'package:flutter_http_requests/pages/products_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(),
      
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text('Ali Baba Market',style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),),
      ),
      body: Center(
        child: ElevatedButton.icon(
          icon: const Icon(
            Icons.chevron_right,
            color: Colors.blue,
            size: 24.0,
          ),
          label: const Text('Products', 
          style: TextStyle(
            color: Colors.black,
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
          ),
          onPressed: () {
            Navigator.of(context).push(
            MaterialPageRoute(builder: (_) => const ProductsPage()),
          );
          },
        )
      )
    );
  }
}
