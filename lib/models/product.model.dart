class Product {
  Product(
      {required this.name,
      required this.description,
      required this.price,
      required this.image});

  final String name;
  final String description;
  final int price;
  final String image;

// factory: permet de recurer a partir de json fournie des obkets de type product;
  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
        name: json['name'],
        description: json['description'],
        price: json['price'],
        image: json['image']);
  }

// getter;
  String get getName => name;
  String get getDescription => description;
  int get getprice => price;
  String get getImage => image;

}

class ProductList {
  // constructor;
  ProductList({
    required this.products,
  });

// prppriété requise par le constructor;
  final List<Product> products;

  factory ProductList.fromJson(List<dynamic> parseJson) {
    List<Product> products = <Product>[];
    products = parseJson.map((p) => Product.fromJson(p)).toList();
    return ProductList(products: products);
  }

}
